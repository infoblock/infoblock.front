export function parseData(data: string|null): string {
    if (data===null) return '-'

    const inputDate = new Date(data);

    const formatter = new Intl.DateTimeFormat('ru', {
        day: "2-digit",
        month: "2-digit",
        year: "numeric",
        hour: "2-digit",
        minute: "2-digit",
    });

    return formatter.format(inputDate).replace(/,/g, "");
}