import axios from 'axios';
import {DataResponse, NewsResponse} from '../constants/types';


const instance = axios.create({
    baseURL: 'http://51.250.93.99/api/',
})

const getNews = async ({page, count}: {page: string, count:string}) => {
    const response = await instance.get<NewsResponse>(`infoblock/news?page=${page}&count=${count}`);
    return response.data;
};

const getItemNews = async (id: string) => {
    const response = await instance.get<DataResponse>(`infoblock/news/${id}`);
    return response.data;
};

const NewsService = {
    getNews,
    getItemNews
};

export default NewsService;