class URLManagerStore {
    public getHomeURL() {return '/'}
    public getNewsItemURL() {return 'news/:id'}
}

const URLManager = new URLManagerStore();
export { URLManager };