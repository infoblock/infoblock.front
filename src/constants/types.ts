export interface DataResponse {
    id: string,
    title: string,
    link: string,
    description: string,
    featureFlag: string,
    isActive: true,
    previewsCount: 0,
    viewsCount: 0,
    avatar: string,
    body: string,
    author: string,
    readTime: 0,
    tags: string[],
    audienceTypes: string[],
    locations: string[],
    createDate: string,
    publicationDate: string,
    editeDate: string,
}

export interface NewsResponse{
    foundEntities: DataResponse[],
    totalCount: number,
}

export interface StateNews {
    foundEntities: DataResponse[],
    totalCount: number,
    item: DataResponse | null,
    page: 1,
    count: 10,
    hasMore: boolean
}
