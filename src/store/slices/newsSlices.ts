import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {DataResponse, StateNews} from "../../constants/types";
import NewsService from "../../services/news.srvice";


const INITIAL_STATE:StateNews = {
    foundEntities: [],
    item: null,
    totalCount: 0,
    page: 1,
    count: 10,
    hasMore: true
};

export const getNewsThunk = createAsyncThunk(
    'news/getNews',
    async (_, {getState, rejectWithValue}) => {
        const state: any = getState();
        try {
            const data = await NewsService.getNews({page: state.news.page, count: state.news.count});
            return data;
        } catch (error: any) {
            return rejectWithValue(await error.json());
        }
    }
);

export const getItemNewsThunk = createAsyncThunk(
    'news/getItemNews',
    async (id: string, {getState, rejectWithValue}) => {
        try {
            const data = await NewsService.getItemNews(id);
            return data;
        } catch (error: any) {
            return rejectWithValue(await error.json());
        }
    }
);

const newsSlice = createSlice({
    name: 'news',
    initialState: INITIAL_STATE,
    reducers: {
        incrementPage: (state) => {
            state.page += 1;
        },
        updateNews: (state) => {
            state.foundEntities = []
            state.page = 1
        },

    },
    extraReducers: (builder) => {
        builder
            .addCase(getNewsThunk.fulfilled, (state, action) => {
                if (action.payload.foundEntities.length < state.count) state.hasMore = false;
                state.totalCount = action.payload.totalCount;
                state.foundEntities.push(...action.payload.foundEntities as DataResponse[]);
                state.foundEntities = state.foundEntities.filter((value, index, self) => self.findIndex(obj => obj.id === value.id) === index);
            })
            .addCase(getNewsThunk.rejected, (state, action) => {
                console.error('Error in getNewsThunk')
            })
            .addCase(getItemNewsThunk.fulfilled, (state, action) => {
                if (action.payload.link === null) state.item = action.payload
            })
            .addCase(getItemNewsThunk.pending, (state, action) => {
                state.item = null
            })
            .addCase(getItemNewsThunk.rejected, (state, action) => {
                console.error('Error in getItemNewsThunk')
            });
    },
});

export const { incrementPage, updateNews } = newsSlice.actions;
export default newsSlice.reducer;