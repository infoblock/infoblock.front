import React from 'react';
import { Provider } from 'react-redux';
import { Route, Routes } from 'react-router-dom';
import { URLManager } from './shared/url-manager';
import Home from "./pages/home/home";
import store from './store/store';
import NewsItem from "./pages/news/newsItem";


function App() {
    return (
        <Provider store={store}>
            <Routes>
                <Route path={URLManager.getHomeURL()} element={<Home />}/>
                <Route path={URLManager.getNewsItemURL()} element={<NewsItem />}/>
                <Route path="*" element={<div>No page</div>}/>
            </Routes>
        </Provider>
    );
}

export default App;