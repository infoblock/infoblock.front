import {FC, useEffect} from "react";
import {Link, useParams} from "react-router-dom";
import parse from "html-react-parser";
import {AiOutlineEye} from "react-icons/ai";
import {BsArrowLeft} from "react-icons/bs";
import {useAppDispatch, useAppSelector} from "../../shared/hooks";
import {getItemNewsThunk} from "../../store/slices/newsSlices";
import style from './newsItem.module.css';
import {URLManager} from "../../shared/url-manager";
import {parseData} from "../../helpers/parseDate";

const NewsItem: FC = () => {
    const {id} = useParams();
    const dispatch = useAppDispatch();
    const news = useAppSelector(state => state.news.item);

    useEffect(()=>{
        id && dispatch(getItemNewsThunk(id))
    }, [])

    return <>
        <div className={style.content}>
            <div className={style.headButtons}>
                <div className={style.btns}>
                    <Link to={URLManager.getHomeURL()} className={style.btnDefault} ><BsArrowLeft style={{transform: 'translateY(2px)'}}/> Назад</Link>
                </div>
            </div>


            {news &&
                (<div>
                    <h1>{news.title}</h1>
                    <div className={style.descriptions}>
                        <div>{parseData(news.publicationDate)}</div>
                        <div className={style.viewsCount}><AiOutlineEye style={{fontSize: '22px'}}/> {news.viewsCount}</div>
                    </div>
                    <div className={style.tags}>{news.tags.map((t, index) => <div className={style.tag} key={index}>{t}</div>)}</div>
                    {news.body && parse(news.body)}
                    <div>
                        <div>Аудитория: <ul>{news.audienceTypes.map((a,index) => <li key={index}>{audienceTranslate[a]} </li>)}</ul></div>
                        <div>Расположение: <ul>{news.locations.map((a,index) => <li key={index}>{locationTranslate[a]} </li>)}</ul></div>
                    </div>
                    <h1 className={style.author}>Автор: {news.author}</h1>
                </div>)
            }
        </div>
    </>
}

export default NewsItem;

const locationTranslate: {[key: string]: string} = {
    "popular": "популярное",
    "main": "главное",
    "useful": "полезное",
    "news": "новости"
}
const audienceTranslate: {[key: string]: string} = {
    "all": "Все",
    "abonent": "Абоненты",
    "portUser": "Портальные пользователи",
    "groupOrg": "Группа организаций"
}