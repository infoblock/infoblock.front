import React, {useEffect} from 'react';
import InfiniteScroll from "react-infinite-scroll-component";
import {getNewsThunk, incrementPage} from "../../store/slices/newsSlices";
import {useAppDispatch, useAppSelector} from "../../shared/hooks";
import NewsCard from "../../components/newsCard/newsCard";
import {DataResponse} from "../../constants/types";
import styles from './homeScroll.module.css';


const Home = () => {
    const dispatch = useAppDispatch();
    const newsState = useAppSelector((state) => state.news)
    const dataNewsArray: DataResponse[] = Object.assign([], Object.values(newsState.foundEntities))

    useEffect(() => {
        dispatch(getNewsThunk())
    }, [])

    return (
        <div className={styles.homePage}>
                <InfiniteScroll
                    dataLength={dataNewsArray.length}
                    next={() => {
                        dispatch(incrementPage())
                        dispatch(getNewsThunk())
                    }}
                    hasMore={newsState.hasMore}
                    loader={<h4>Loading...</h4>}
                >
                    {dataNewsArray.map(news => <NewsCard key={news.id} {...news}/>)}
                </InfiniteScroll>
        </div>
    );
};

export default Home;
