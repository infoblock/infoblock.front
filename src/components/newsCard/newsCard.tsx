import React from 'react';
import {DataResponse} from '../../constants/types';
import styles from './newsCard.module.css';
import {useNavigate} from "react-router-dom";
import {getItemNewsThunk} from "../../store/slices/newsSlices";
import {useAppDispatch} from "../../shared/hooks";


const NewsCard = (news: DataResponse) => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const openItemNews = (news: DataResponse) => {
        if (news.link){
            window.open(news.link, '_blank')
            dispatch(getItemNewsThunk(news.id))
        } else {
            navigate(`/news/${news.id}`);
        }
    }

    return (
        <div className={styles.news} onClick={() => openItemNews(news)}>
            <div className={styles.avatar} style={{backgroundImage: `url(${news.avatar})`}}></div>
            <div className={styles.newsTitleDate}>
                <div className={styles.newsTitle}>{news.title}</div>
                <div className={styles.newsDate}>{dateTranslate(news.publicationDate)}</div>
            </div>
            <div className={styles.newsContent}>
                <div className={styles.tags}>{news.tags.map((t, index) => <span key={index}>{t}</span>)}</div>
                {news.description}
            </div>
        </div>
    );
};

export default NewsCard;


const dateTranslate = (data: string) => {
    const date = new Date(data);
    return date.toLocaleDateString()
}